import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import uuid from 'uuid4';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import reducers from './src/reducers';
import WeatherComponent from './src/components/WeatherComponent';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = { widgets: [] };
    }

    _addWidget() {
        const { widgets } = this.state;
        widgets.push({ id: uuid() });
        this.setState({ widgets });
    }

    render() {

        return (
            <div>
                <div>
                    <a href="javascript:void(0);" onClick={this._addWidget.bind(this)}>Add Widget</a>
                    {this.state.widgets.map((widget, index) => <WeatherComponent key={index} id={widget.id}/>)}
                </div>
            </div>
        )
    }
}

ReactDOM.render(
    <Provider store={ createStore(reducers, {}, applyMiddleware(ReduxThunk)) }>
        <App />
    </Provider>
    , document.querySelector('.container'));