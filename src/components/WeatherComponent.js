import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { loadCities } from '../actions/WeatherActions';

class WeatherComponent extends Component {

    componentWillMount() {

        const { id, loadCities } = this.props;
        loadCities(id);
    }

    render() {

        const { loading, cities } = this.props.weather;

        if(loading) {
            return <div>Loading....</div>
        }

        return <div>{JSON.stringify(cities) }</div>;
    }
}

const mapStateToProps = state => {

    return { weather: state.weatherReducer }
};

const mapDispatchToProps = dispatch => {
    return bindActionCreators({ loadCities }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(WeatherComponent);