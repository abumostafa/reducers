import uuid from 'uuid4';
import {
    WEATHER_LOAD,
    WEATHER_LOAD_SUCCESS,
} from './types';

export const loadCities = (id) => {

    return (dispatch) => {

        dispatch({ type: WEATHER_LOAD });

        // simulate ajax request and return array of cities
        return dispatch({ type: WEATHER_LOAD_SUCCESS, payload: [uuid(), uuid()] });
    };
};