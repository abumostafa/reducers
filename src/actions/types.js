export const WEATHER_LOAD           = 'weather_load';
export const WEATHER_LOAD_ERROR     = 'weather_load_error';
export const WEATHER_LOAD_SUCCESS   = 'weather_load_success';