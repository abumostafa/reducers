import { combineReducers } from 'redux';
import {
    WEATHER_LOAD,
    WEATHER_LOAD_ERROR,
    WEATHER_LOAD_SUCCESS,
} from '../actions/types';

const INITIAL_STATE = {
    cities: [],
    loading: true,
};

const WeatherReducer = (state = INITIAL_STATE, action) => {

    const { type, payload } = action;

    switch (type) {

        case WEATHER_LOAD:
            return { ...state, loading: true };

        case WEATHER_LOAD_ERROR:
            return { ...state, loading: null, err: payload };

        case WEATHER_LOAD_SUCCESS:
            return { ...state, loading: false, cities: payload };

        default:
            return state;
    }
};

export default combineReducers({

    weatherReducer: WeatherReducer
})